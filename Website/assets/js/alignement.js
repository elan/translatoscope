let translatoscope = {
    fontSizes: ['x-small', 'small', 'medium', 'large', 'x-large'],
    currentSizeIndex: 1,
    init: function () {
        // Get all second level dropdown items and attach click event listener
        const dropdownItems = document.querySelectorAll('.dropdown-menu > .dropdown-item > .dropdown-menu > .dropdown-item')
        dropdownItems.forEach(item => {
            item.addEventListener('click', function (event) {
                const auteur = this.textContent.trim()
                console.log("auteur : " + auteur)
                const pieceJson = this.closest('.dropdown-menu').parentNode.getAttribute('id')
                const parentLi = item.closest('.dropdown-submenu')
                // Logging the text content of the parent <li> element
                const piecePropre = parentLi.firstChild.textContent.trim()
                translatoscope.addNewCard(pieceJson, piecePropre, auteur)
            });
        });

        // attach click event on fontsize buttons
        document.getElementById('btn_increase_font').addEventListener('click', function () {
            // Increase the font size index, ensuring it doesn't go out of bounds
            translatoscope.currentSizeIndex = Math.min(translatoscope.currentSizeIndex + 1, translatoscope.fontSizes.length - 1)
            translatoscope.adjustFontSizes()
        });

        document.getElementById('btn_decrease_font').addEventListener('click', function () {
            // Decrease the font size index, ensuring it doesn't go out of bounds
            translatoscope.currentSizeIndex = Math.max(translatoscope.currentSizeIndex - 1, 0)
            translatoscope.adjustFontSizes()
        });

        // --- Show Ploutus when page loads--- //
this.addNewCard("Ploutos","Ploutos","1930 - VanDaele")
    },
    addNewCard: function (pieceJson, piecePropre, auteur) {
        // // Extract the part after ' - '
        // const auteurWithoutDate = auteur.split(' - ')[1]
        // Split the string by ' - '
        const parts = auteur.split(' - ');

        // If there are multiple parts (meaning there is a hyphen), use the second part
        // Otherwise, use the entire original string
        const auteurWithoutDate = parts.length > 1 ? parts[1] : auteur;
        
        // Replace spaces with underscores
        const auteurWithUnderscores = auteurWithoutDate.replace(/\s+/g, "_")

        
        filename = 'content/'+ pieceJson + "_" + auteurWithUnderscores + ".json";
        console.log(`Trying to fetch "${filename}"`);

        fetch(filename)
            .then(response => {
                if (!response.ok) {
                    throw new Error("Couldn't fetch"+filename);
                }
                return response.json();
            })
            .then(data => {
                
                json_key = pieceJson+ "_" + auteurWithUnderscores
                const content = data[json_key]

                // Create a new card element
                const newCard = document.createElement('div')
                newCard.classList.add('col-lg-3', 'col-md-4', 'col-sm-6', 'gx-1')
                title = piecePropre + " - " + auteur
                // Create card HTML content
                let cardHTML = `
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                                <h5 class="card-title">${title}</h5>
                                <button type="button" class="btn-close" aria-label="Close"></button>
                            </div>
                        <ul class="list-group list-group-flush">
                `

                // Loop through the data and create list items for each item
                content.forEach(item => {
                    // Check if the item has nested content (indicated by an array within the third position) --> for the magenta case
                    if (Array.isArray(item[2])) {
                        const [id, color] = item // Extract id and color from the item
                        let nestedHTML = `<li id="${id}" class="list-group-item ${color}">`
                        // Combine all nested speaker/text pairs into one line's HTML
                        item[2].forEach(([title, text]) => {
                            nestedHTML += `<h5>${title} :</h5>${text} <br> <br>`;
                        });
                        nestedHTML += '</li>'
                        cardHTML += nestedHTML
                    } else {
                        const [id, color, title, text] = item
                        cardHTML += `
                            <li id="${id}" class="list-group-item ${color}">
                            <div class="row">
                                <div  class="col-auto hidden-index hidden" style="display: none;">
                                    ${id}
                                </div>
                                <div class="col">
                                    <h5>
                                        ${title} : 
                                    </h5>
                                </div>
                            </div>
                                ${text}
                            </li>
                        `
                    }
                });

                // Close the card HTML content
                cardHTML += `
                        </ul>
                    </div>
                `

                // Set the HTML content to the new card
                newCard.innerHTML = cardHTML;
                // 'col-lg-3','col-md-4','col-md-3', 'gx-1'

                const row = document.querySelector('.row-cards');
                row.appendChild(newCard);

                translatoscope.bindHoverEffects();

                // Call adjustFontSizes to set the initial font sizes
                translatoscope.adjustFontSizes();

                // Select the CLOSE BUTTON  of the newly created card
                const closeButton = newCard.querySelector('.btn-close');

                // Add click event listener to the close button
                closeButton.addEventListener('click', function () {
                    // Find the parent column element
                    const column = this.closest('.col-lg-3, .col-md-4, .col-sm-6');
                    if (column) {
                        // Remove the column from the DOM
                        column.remove();
                    }
                });   // Bind hover effects to new lines
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });

    },
    applyHoverEffect: function (color, currentLine) {
        // Apply hover effect to the currentLine if it has color class
        if (currentLine.classList.contains(color)) {
            currentLine.classList.add('hover-effect');
        }

        // Check the line above/previous if it has color class
        let previousLine = currentLine.previousElementSibling;
        while (previousLine) {
            // if it does we apply the hover-effect to it
            if (previousLine.classList.contains(color)) {
                previousLine.classList.add('hover-effect');
                previousLine = previousLine.previousElementSibling; // Move to the next line above
            } else {
                break; // Stop if the line is not color
            }
        }

        // Traverse downwards from the currentLine and apply hover effect to color lines
        let nextLine = currentLine.nextElementSibling;
        while (nextLine) {
            if (nextLine.classList.contains(color)) {
                nextLine.classList.add('hover-effect');
                nextLine = nextLine.nextElementSibling; // Move to the next line below
            } else {
                break; // Stop if the line is not color
            }
        }
    },

    // Returns a function that delays invoking 'func' until after 'wait' milliseconds have elapsed since the last call.
    debounce: function (func, wait) {
        let timeout; // Declare a variable to store the timeout ID

        return function (...args) { // Return a new function that will be called instead of the original function
            const context = this; // Preserve the context (value of `this`) of the caller

            clearTimeout(timeout); // Clear any existing timeout to reset the debounce period

            // Set a new timeout to call the function after the specified wait period
            timeout = setTimeout(() => func.apply(context, args), wait);
        };
    },
    bindHoverEffects: function () {
        // Attach event listeners to all list-group-item elements within all cards
        document.querySelectorAll('.card .list-group-item').forEach((line) => {
            // When the mouse moves over a list-group-item...
            line.addEventListener('mousemove', this.debounce(function () {
                // Get the current card containing this list-group-item
                const currentCard = this.closest('.list-group-flush');
                // Find the index of the hovered list-group-item within its card
                const lineId = this.id;
                // console.log("Test scroll");

                // Remove hover-effect from all lines
                document.querySelectorAll('.card .list-group-item').forEach(function (line) {
                    line.classList.remove('hover-effect');
                });

                document.querySelectorAll('.card .list-group-flush').forEach(function (card) {
                    const correspondingLine = card.querySelector(`.list-group-item[id="${lineId}"]`);
                    if (correspondingLine) {
                        correspondingLine.classList.add('hover-effect');
                        if (correspondingLine.classList.contains('Bleu')) {
                            translatoscope.applyHoverEffect("Bleu", correspondingLine);
                        } else if (correspondingLine.classList.contains('Marron')) {
                            translatoscope.applyHoverEffect("Marron", correspondingLine);
                        }
                    }
                });

                // For each card other than the current card...
                document.querySelectorAll('.card .list-group-flush').forEach(function (card) {
                    // Check if it's not the current card to avoid scrolling the card you're interacting with
                    if (card === currentCard) return; // If it is the current card, do nothing (skip it)

                    // Again, find the corresponding line by index in other cards
                    const correspondingLine = card.querySelector(`.list-group-item[id="${lineId}"]`);
                    // If such a line exists in the other card...
                    if (correspondingLine) {
                        // Calculate the scroll amount needed to align the top of the correspondingLine with the top of the line
                        const lineTop = line.getBoundingClientRect().top; // Get the top position of the line relative to the viewport
                        const correspondingLineTop = correspondingLine.getBoundingClientRect().top; // Get the top position of the corresponding line relative to the viewport
                        const scrollAmount = correspondingLineTop - lineTop; // Calculate the scroll amount
                        // Apply the calculated scroll amount to the card
                        card.scrollTop += scrollAmount;
                    }
                });
            }, 10).bind(line)); // Adjust the debounce delay as needed (in milliseconds)

            // When the mouse leaves a line...
            line.addEventListener('mouseleave', function () {
                // Remove the hover-effect class from all lines in all cards
                document.querySelectorAll('.card .list-group-item').forEach(function (line) {
                    line.classList.remove('hover-effect');
                });
            });
        });
    },

    adjustFontSizes: function () {
        console.log(translatoscope.currentSizeIndex);
        // Iterate through each list item
        document.querySelectorAll('.list-group-item').forEach(function (item) {
            // Set the font size for each <h5> element and its text content
            item.querySelectorAll('h5').forEach(function (h5Element) {
                h5Element.style.fontSize = translatoscope.fontSizes[translatoscope.currentSizeIndex];
            });
            item.childNodes.forEach(function (node) {
                if (node.nodeType === Node.TEXT_NODE) {
                    node.parentElement.style.fontSize = translatoscope.fontSizes[translatoscope.currentSizeIndex];
                }
            });
        });
    }
}

window.addEventListener('load', (event) => {
    translatoscope.init()
})
