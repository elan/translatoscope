Lien vers le site ouvert : https://elan.gricad-pages.univ-grenoble-alpes.fr/translatoscope/

Lien vers le site restreint (besoin d'être membre du projet) : https://elan.gricad-pages.univ-grenoble-alpes.fr/translatoscope-restreint/

Pour ajouter les données d'alignements aux fichiers XML ou pour mettre à jour les données du site à partir des données d'alignement, voir Documentation/relecture_xml_tei

Le dossier XML_alignés contient les fichiers XML tels que présents sur le site auxquels ont été ajoutés les données d'alignement. 

# Sommaire

1. Présentation
2. Installation

   * Télécharger Git
   * Télécharger Python 3
   * Télécharger LibreOffice Calc
   * Télécharger Visual Studio Code
   * Se créer un compte dans Gricad-Gitlab
   * Vidéo configuation Visual Studio Code
3. Utilisation du fichier 1_XML_to_xlsx.ipynb

   * En théorie
   * En pratique
4. Utilisation du fichier 2_align_on_xlsx.ipynb

   * En théorie
   * En pratique
5. Utilisation du fichier 3_xlsx_to_JSON.ipynb

   * En théorie
   * En pratique
6. Accepter une merge-request
7. Récapitulatif complet

# Présentation

L'objectif de ce dépôt est de proposer un outil d'alignement de fichier XML-TEI de pièces de théâtre afin d'aligner des traductions et d'ajouter les pièces alignées à un site web pour les parcourir facilement.

Pour l'instant, les pièces alignées sont des traductions de pièces de théâtre d'Aristophane, alignées sur le texte grec établi par Coulon.

Le protocole d'alignement se compose de quatre étapes :

1. installation des logiciels nécessaires, clonage du projet
2. **premier algorithme** (1_XML_to_xlsx.ipynb) qui transforme un fichier XML-TEI en un fichier xlsx (tableau type OpenOfficeCalc/Excel)
3. **second algorithme** (2_align_on_xlsx.ipynb) qui permet d'aligner une traduction avec le texte original
   - à l'aide du logiciel OpenOffice Calc
4. **troisième algorithme** (3_xlsx_to_JSON.ipynb) qui ajoute le texte aligné au site web

---

!! Prenez bien le temps de copier **TOUTES** les manipulations qui sont dans les vidéos !!

# Installation

Téléchargez les applications en fonction de votre système d'exploitation (Windows, Mac, Linux).

Dans chacun des cas, suivez l'installation en choisissant les valeurs par défaut.

Sommaire :

- Télécharger Git
- Télécharger Python 3
- Télécharger LibreOfficeCalc
- Télécharger Visual Studio Code
- Se créer un compte sur Gricad-Gitlab puis demander les droits (par mail) pour avoir accès au projet "Translatoscope-restreint"

---

## Télécharger Git

#### Si vous utilisez Windows :

Allez sur : [https://git-scm.com/downloads](https://git-scm.com/downloads), téléchargez la version "64-bit for Windows Setup" et suivez toutes les valeurs par défaut lors de l'installation.

<img src="Documentation/Installation/InstallGit.png" alt="Télécharger Git" width="800" height="400">

#### Si vous utilisez Linux :

Allez dans l'application "Terminal" puis écrivez :

```bash
sudo apt-get install git
```

Il est possible que cette installation demande le mot de passe de votre ordinateur/session, attention les lettres/chiffres ne seront pas visibles quand vous les écrirez.

Vous pouvez vérifier si Git est bien installé sur votre ordinateur en tapant dans le terminal :

```bash
git --version
```

#### Si vous utilisez un Mac :

Allez dans l'application "Terminal" puis écrivez :

```bash
xcode-select --install
```

puis suivre les indications d'installation par défaut.

Vous pouvez vérifier si Git est bien installé sur votre ordinateur en tapant dans le terminal :

```bash
git --version
```

si un message comme "git version 2.30.1" s'affiche, Git est installé

Si vous voyez encore un message d'erreur, il faudra essayer de télécharger Git via HomeBrew, écrivez cette ligne dans le terminal :

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Il est possible que cette installation demande le mot de passe de votre ordinateur/session, attention les lettres/chiffres ne seront pas visibles quand vous les écrirez.

Ensuite, une fois HomeBrew installé, vous pouvez installer Git en écrivant cette ligne dans le terminal :

```bash
brew install git
```

Normalement Git est installé sur votre ordinateur, vous pouvez le vérifier en tapant dans le terminal :

```bash
git --version
```

## Télécharger Python 3

Télécharger la dernière version de python 3 associé à votre OS (Windows/Mac/Linux), en allant à cette adresse : [https://www.python.org/downloads/](https://www.python.org/downloads/)

Choisir toutes les valeurs par défaut pendant l'installation.

<img src="Documentation/Installation/InstallPython.png" alt="Télécharger python" width="800" height="400">

---

## Télécharger LibreOffice Calc

Télécharger LibreOffice (Calc) à cette adresse : [https://fr.libreoffice.org/download/telecharger-libreoffice/](https://fr.libreoffice.org/download/telecharger-libreoffice/)

ll faut d'abord télécharger la version LibreOffice complète puis dans un second temps, lors de l'installation, il est possible de choisir si on  ne souhaite télécharger que Writer (Word), Calc (Excel), Draw...

<img src="Documentation/Installation/InstallLibreOffice.png" alt="Télécharger LibreOffice" width="800" height="400">

---

## Télécharger Visual Studio Code

Téléchargez Visual Studio Code à cette adresse : https://code.visualstudio.com/Download

Choisir toutes les valeurs par défaut pendant l'installation.

<img src="Documentation/Installation/InstallVSC.png" alt="alt text" width="800" height="400">

---

#### Se créer un compte dans Gricad-Gitlab

Créez-vous un compte sur Gricad-Gitlab (vous pouvez vous connectez avec vos identifiants universitaires) : https://gricad-gitlab.univ-grenoble-alpes.fr/users/sign_in

Une fois crée, vous pouvez demander (par mail) à faire partie du projet "Translatoscope-restreint".

<img src="Documentation/Installation/LogInGitLab.png" alt="Se créer un compte dans Gricad-Gitlab" width="800" height="400">

### IMPORTANT : Une fois tout téléchargé, il est nécessaire suivre toutes les indications de la vidéo ci-dessous pour configurer Visual Studio Code :

[Finir l&#39;installation dans Visual Studio Code](https://videos.univ-grenoble-alpes.fr/video/30178-finalisation-de-linstallation-dans-visual-studio-code/)

Commandes pour Mac/Linux :

```terminal
python3 -m ensurepip --upgrade
python3 -m pip install -r requirements.txt
```

Commandes pour Windows :

```terminal
python -m ensurepip --upgrade
python -m pip install -r requirements.txt
```

Après avoir configuré Visual Studio Code, vous pouvez utiliser le code !

# 1. Tutoriel pour l'utilisation du fichier 1_XML_to_xlsx.ipynb

!! A noter que, si ça ne marche pas, et cela vaut pour l'ensemble des codes, c'est à 95% une erreur d'orthographe quand vous écrivez les variables oeuvre/auteur !!

## En théorie

Etape très facile !

Le fichier 1_XML_to_xlsx.ipynb contient un ensemble de codes qui permettent de transformer un fichier XML/TEI en un tableau LibreOfficeCalc/Excel (.xlsx).

Faites bien attention à la manière dont vous écrivez le nom du fichier (pièce__auteur) et la manière dont vous écrivez l'auteur et la pièce dans le code, ils doivent être identiques !
Si vous ajoutez une pièce qui existe déjà, faites attention à bien la nommer telle qu'elle est déjà nommée dans le nom de dossier/fichiers.

## En pratique

Suivez le guide en vidéo :

[Tutoriel vidéo pour la première étape du protocole](https://videos.univ-grenoble-alpes.fr/video/30494-tutoriel-1_xml_to_xlsx/ "https://videos.univ-grenoble-alpes.fr/video/30494-tutoriel-1_xml_to_xlsx/")

Ou à l'écrit :

##### Avant de commencer :

Assurez vous d'avoir bien mis à jour votre projet local avec le projet distant en faisant la commande "git pull origin main" (voir la vidéo de 2:50 à 4:00)

##### Première étape : Charger le fichier XML dans le projet

- Dans votre gestionnaire de fichier (Finder sur Mac) allez à l('endroit où vous avez cloné le projet "Translatoscope-restreint"
- Allez dans le dossier XML, puis dans le dossier correspondant au nom de la pièce
- Déposez ici le fichier xml de la pièce que vous voulez traiter
- Renommez le fichier en lui donnant le même nom de pièce que le nom du dossier contenant les pièces
- Séparez le nom de la pièce et le nom de l'auteur par 2 tirets du bas "__"
- Remplacez les espaces par des tirets du bas "_"
- L'important est que vous choisissiez un nom que vous réécrirez tel quel dans les fichiers de code

![Charger le XML](Documentation/1_XML_to_xlsx/charger_XML.png)

##### Deuxième étape :

Dans Visual Studio Code, allez dans le fichier "Code", puis double-cliquez sur le fichier 1_XML_to_xlsx.ipynb.

- Ecrire le nom de la pièce entre les parenthèses (oeuvre = "...")
- Ecrire le nom de l'auteur, tel qu'il est écrit dans le nom de fichier .xml, entre les parenthèses (auteur = "...")
- Spécifier si le texte est en vers ou non
  - si oui préciser la balise de retour à la ligne

![Modifier les informations](Documentation/1_XML_to_xlsx/XML_to_xlsx_1.png)

##### Troisième étape :

- Cliquer sur le bouton "run all" au dessus des valeurs que vous avez modifiées
- Normalement, un nouveau fichier a été créé dans le dossier "Alignement/Nom_de_la_pièce/" et il a pour nom "alignement_piece_auteur.xlsx"
- Si le fichier a été créé, vous avez réussi et pouvez passer à l'étape suivante !
- Sinon, soit vous avez mal écrit le nom de la pièce/auteur, soit votre fichier XML comporte une irrégularité non-prévue par le code

![Le ficher xlsx a été crée](Documentation/1_XML_to_xlsx/XML_to_xlsx_2.png)

# 2. Tutoriel pour l'utilisation du fichier 2_align_on_xlsx.ipynb

## Théorie

Le fichier 2_align_on_xlsx.ipynb contient un ensemble de codes qui permettent d'aligner des traductions à l'aide du logiciel LibreOffice Calc.

La première pièce à aligner peut sembler longue (la vidéo aussi ..!), mais cela diminue très rapidement dès la seconde pièce car les opérations à mener sont assez répétitives !

## En pratique

Voici un tutoriel vidéo qui détaille l'ensemble des problématiques que l'on rencontre pendant cette étape, et qui montre comment aligner un texte de A à Z :

[Lien vers la vidéo](https://videos.univ-grenoble-alpes.fr/video/30579-tutoriel-2_align_on_xlsx/ "https://videos.univ-grenoble-alpes.fr/video/30579-tutoriel-2_align_on_xlsx/")

N'hésitez pas à mettre la vidéo en accéléré (surtout lorsque je donne seulement plusieurs exemples des mêmes cas de code couleur/dictionnaire)

* Sommaire de la vidéo :

Début - 1:15 - Présentation du code <br>
1:15 - 4:50 - Présentation fichier xlsx / Réglage visuel / Raccourci clavier <br>
4:50 - 6:25 - Explication dictionnaire de concordance <br>
6:25 - 8:00 - Copier/Coller le dictionnaire de concordance <br>
8:00 - 9:20 - Mise à jour dictionnaire de concordance <br>
9:20 - 10:30 - Première exécution de l'algorithme <br>
10:30 - 11:40 - Explication rapide de ce que fait l'algorithme <br>
11:40 - 12:30 - Premier problème / Mise à jour dictionnaire de concordance (+ astuce)<br>
12:30 - 14:10 - Problème orthographe du nom de personnage dans la traduction <br>
16:10 - 17:45 - Crochets dans le dictionnaire de concordance [ ]<br>
17:45 - 19:15 - Introduction code couleur <br>
19:15 - 22:05 - Cas Orange <br>
22:05 - 26:10 - Cas Bleu <br>
26:10 - 31:50 - Cas Violet <br>
31:50 - 34:45 - Mise à jour dictionnaire de concordance <br>
34:45 - 37:55 - Cas Marron <br>
37:55 - 39:20 - Mise à jour dictionnaire de concordance <br>
39:20 - 40:25 - Problème orthographe du nom de personnage dans la traduction <br>
40:25 - 41:00 - Mise à jour dictionnaire de concordance <br>
41:00 - 43:15 - Cas Bleu <br>
43:15 - 44:40 - Cas Orange <br>
44:40 - 47:30 - Problème de balises dans le fichier XML <br>
47:30 - 48:00 - Cas Orange <br>
48:00 - 49:10 - Mise à jour dictionnaire de concordance <br>
49:10 - 50:40 - Cas Gris <br>
50:40 - 53:40 - Cas Rose <br>
53:40 - 54:40 - Mise à jour dictionnaire de concordance <br>
54:40 - 58:00 - Crochets dans le dictionnaire de concordance [] // Cas Orange <br>
58:00 - 59:30 - Supprimer dernières lignes // Sauvegarder le dictionnaire <br>
59:30 - Fin - Conseils <br>

De plus, voici le tutoriel écrit qui retrace l'ensemble des problématiques (couleurs) ainsi que leur résolution :

[Aide mémoire pour l&#39;alignement](Documentation/code_couleur.md)

Note :

- Pensez à bien ouvrir le fichier .xlsx avec LibreOffice (clique droit --> Ouvrir avec --> LibreOffice(calc) )
- Pensez à bien "exécuter la cellule" du dictionnaire de concordance après l'avoir modifié
- Pensez à bien sauvegarder tout changement dans LibreOffice avant de relancer l'algorithme
- Pensez à bien relancer l'algorithme d'alignement pour rafraichir les changements sur le fichier xlsx

# 3. Tutoriel pour l'utilisation du fichier 3_xlsx_to_JSON.ipynb

## En théorie

Après avoir aligné la traduction avec le script précédent (2_align_on_xlsx.ipynb), il faut utiliser le fichier "3_xlsx_to_JSON.ipynb".
Ce fichier contient un ensemble de codes Python permettant d'ajouter la traduction alignée à un fichier .json qui est facilement lu par le site-web.
Il faut aussi faire quelques réglages à la main pour :

- ajouter le nouveau texte au bouton de choix des textes (dans le fichier "Website/alignement.html")
- ajouter la référence bibliographique du texte (dans le fichier "Website/source.html")
- mettre à jour le fichier des droits d'ouverture ("droit_ouverture.csv")

Le code "ajouter un texte à Website/content/xxx.json" prend le contenu du fichier .xlsx actuel (en fonction des variables oeuvre/auteur) et crée un fichier .json. Si le fichier .json existe déjà, il sera remplacé avec les données du fichier xlsx actuel.

## En pratique

Voici un tutoriel vidéo qui montre et explique les étapes à suivre :

[Lien vers la vidéo](https://videos.univ-grenoble-alpes.fr/video/30492-tutoriel-3_xlsx_to_json/ "https://videos.univ-grenoble-alpes.fr/video/30492-tutoriel-3_xlsx_to_json/")

Sommaire de la vidéo :

Début - 2:15 - Présentation/Utilisation du code <br>
2:15 - 5:15 - Fichier "droit_ouverture.csv"<br>
5:15 - 7:25 - Fichier "alignement.html"<br>
7:25 - 10:25 - Fichier "source.html"<br>
10:25 - 12:25 - Explication "source control" et création de branche <br>
12:25 - 14:20 - S'identifier avec "git config"<br>
14:20 - 16:20 - Ecrire un message de "commit" puis "pull"<br>
16:20 - 17:20 - Explication théorique des conflits <br>
17:20 - 18:20 - Résolution conflit "droit_ouverture.csv"<br>
18:20 - 19:20 - Aide générale pour les conflits <br>
19:20 - 21:00 - Résolution conflit "alignement.html"<br>
21:00 - 22:15 - Résolution conflit "source.html"<br>
22:15 - 23:50 - Résolution conflit pour les fichiers de code <br>
23:50 - 24:35 - Ecrire un message de "commit" des conflits <br>
24:35 - 26:30 - Publier et créer une "merge request"<br>
26:10 - Fin - Conseils de fin <br>

# 4. Accepter la merge request (nécessite les droits d'administateur)

Voici un tutoriel qui explique comment accepter une "merge request" sur Gricad-Gitlab :

[Lien vers la vidéo](https://videos.univ-grenoble-alpes.fr/video/30463-tutoriel-accepter-une-merge-request/ "https://videos.univ-grenoble-alpes.fr/video/30463-tutoriel-accepter-une-merge-request/")

# Résumé global

## 1_XML_to_xlsx

- Mettre à jour son projet en faisant "Pull From" puis "Origin Main"
- Ajouter le fichier XML dans le projet au bon endroit
- Renommer le fichier XML de la bonne manière
- Ouvrir le fichier de code "1_XML_to_xlsx.ipynb"
- Modifier les valeurs de la première cellule en fonction de la pièce et de l'auteur
- Appuyer sur "Run All"

## 2_align_on_xlsx

- Ouvrir le fichier de code "2_align_on_xlsx.ipynb"
- Modifier les valeurs de la première cellule en fonction de la pièce et de l'auteur
- Exécuter cette cellule pour mettre à jour les valeurs (triangle à gauche de la cellule)
- Charger un ancien dictionnaire de concordance de la pièce
- Le copier et le coller dans la cellule sous "Créer un nouveau dictionnaire de concordance"
- Utiliser l'algorithme d'alignement pour :
  - mettre à jour le dictionnaire de concordance
  - régler les irrégularités à l'aide du code couleur et du protocole
- Lorsqu'il n'y a plus d'erreur qui apparait sous l'algorithme d'alignement, l'alignement est terminé
- Enlever les lignes inutilisées dans le tableur
- Sauvegarder le dictionnaire de concordance

## 3_xlsx_to_json

- Ouvrir le fichier de code "3_xlsx_to_json.ipynb"
- Modifier les valeurs de la première cellule en fonction de la pièce et de l'auteur
- Exécuter cette cellule pour mettre à jour les valeurs (triangle à gauche)
- Exécuter le groupe "Code"
- Exécuter "Ajouter un texte à Website/content/xxx.json"
- Exécuter "Code pour mettre à jour le fichier des droits d'ouverture"
- Ouvrir le fichier "droit_ouverture.csv" pour compléter les colonnes
- Dans le dossier "Website", ouvrir le fichier "alignement.html" pour ajouter la pièce/auteur
- Dans le dossier "Website", ouvrir le fichier "source.html" pour ajouter la référence bibliographique
- Dans Visual Studio Code, aller dans l'onglet "Source control" dans le menu à gauche
- Créer une branche ou changer de branche si vous n'êtes pas déjà sur la vôtre
- Ecrire un "commit message" et appuyer sur le bouton "Commit"
- Aller sur les petits points puis dans "Pull,Push" puis "Pull from" ("Extraire de"), puis "origin/main"
- Soit vous avez des erreurs et vous devez résoudre les conflits puis appuyer sur le bouton "Publish"
- Soit vous n'avez pas d'erreurs et vous pouvez directement appuyer sur le bouton "Publish"
- Connectez-vous à votre compte sur "Gricad-Gitlab", allez dans le projet "Translatoscope restreint" et cliquez sur "Merge"
- Remplir le formulaire de "Merge request" et envoyer la demande
