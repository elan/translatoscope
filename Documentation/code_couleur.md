# Code couleur

- **Orange** (changement de locuteur - "Orange clair 2")
- **Bleu** (plusieurs répliques chez Coulon/VanDaele deviennent une réplique dans la traduction à aligner - "Bleu clair 2")
- **Gris** (il manque la prise de parole dans la traduction à aligner - "Gris clair 2")
- **Violet** (une réplique chez Coulon/VanDaele devient plusieurs prises de parole dans la traduction à aligner - "Magenta clair 2")
- **Rose** (une prise de parole supplémentaire dans la traduction à aligner par rapport à la version de Coulon/VanDaele - "Magenta clair 4")
- **Marron** (changement dans l'ordre des répliques - "Or foncé 2")

---

# Orange

Changement de locuteur entre la version de Coulon/VanDaele et la traduction à aligner. `<br>`
Pour un nom de personnage constant chez Coulon, le nom de personnage change dans la traduction.

"**Orange clair 2**" dans LibreOfficeCalc

---

Visuel du problème dans LibreOfficeCalc :

![Problème pour le cas orange](image_code_couleur/jaune/jaune_probleme.png)

Dans cet exemple, on voit que les locuteurs sont différents entre la version de VanDaele et la version du texte à aligner :

- CARION pour VanDaele
- KHRÉMYLOS. pour le traducteur à aligner

Pourtant le texte est similaire :

- "Rien d'étonnant ; moi non plus, qui vois clair." chez VanDaele
- "Et cela n’a rien d’étonnant : je n’en vois pas, moi qui vois clair." chez le traducteur à aligner

Cela signifie que le locuteur de la réplique est différent dans la traduction.

Dans ce cas-là, on applique l'arrière plan "Orange clair 2" dans pour les deux cellules.

![Couleur jaune dans LibreOfficeCalc](image_code_couleur/jaune/couleur_jaune.png)

Et le résultat ressemble à ça :

![Solution pour le cas orange](image_code_couleur/jaune/solution_jaune.png)

# Bleu

Plusieurs répliques chez Coulon/VanDaele deviennent une réplique dans la traduction à aligner.

"**Bleu clair 2**" dans LibreOfficeCalc

---

Visuel du problème dans LibreOfficeCalc :

![Visuel du problème dans LibreOfficeCalc](image_code_couleur/bleu/probleme_bleu.png)

Dans cet exemple, on voit que la réplique de CARION chez VanDaele ligne 57 est, chez le traducteur à aligner, intégrée à la réplique de KHRÉMYLOS. ligne 56 :

![Problème expliqué](image_code_couleur/bleu/probleme_bleu_explained.png)

Dans ce cas-là, on crée deux nouvelles cellules sous la réplique de KHRÉMYLOS. et on applique l'arrière-plan "Bleu clair 2" pour les deux cellules :

![Insérer problème bleu](image_code_couleur/bleu/inserer_probleme_bleu.png)

![Décaler cellule bas](image_code_couleur/decaler_cellule_bas.png)
![Couleur bleu](image_code_couleur/bleu/couleur_bleu.png)

Et le résultat ressemble à ça :

![Solution bleue](image_code_couleur/bleu/solution_bleu.png)

# Gris

Ll manque la prise de parole dans la traduction à aligne par rapport à la version de Coulon/VanDaele.

"**Gris clair 2**" dans LibreOfficeCalc

---

Visuel du problème dans LibreOffice Calc :

![Visuel du problème](image_code_couleur/gris/probleme_gris.png)

Dans cet exemple, on voit que, comme dans le cas Bleu, les répliques chez le traducteur à aligner sont une ligne trop élevées :

![Problème gris](image_code_couleur/gris/probleme_gris_explained.png)

Il faut donc créer deux cellules pour les faire s'aligner :

![Insérer gris](image_code_couleur/gris/inserer_gris.png)

![](image_code_couleur/decaler_cellule_bas.png)

![Pré-solution gris](image_code_couleur/gris/gris_pre_solution.png)

Ensuite, il reste à savoir si la réplque de PLOUTOS chez VanDaele ("Je te crois") est présente dans la réplique de KHRÉMYLOS. chez le traducteur à aligner ligne 145.

Ici, il semblerait plutôt que le traducteur a simplement omis cette réplique de Ploutos. On applique donc l'arrière plan "Gris clair 2".

![Couleur gris](image_code_couleur/gris/couleur_gris.png)

Et le résultat ressemble à ça (les répliques qui suivent auront l'arrière-plan vert colorié par l'algorithme):

![Solution gris](image_code_couleur/gris/solution_gris.png)

# Violet

Une réplique chez Coulon/VanDaele devient plusieurs prises de parole dans la traduction à aligner.

"**Magenta clair 2**" dans Libre Office Calc.

---

Visuel du problème dans LibreOfficeCalc :

![Visuel du problème dans LibreOfficeCalc](image_code_couleur/violet/probleme_violet.png)

Dans cet exemple, le cas est plus compliqué. Mais on s'aperçoit vite que les répliques du locuteur à aligner sont deux lignes en dessous de celles de VanDaele, et que trois répliques chez le locuteur à aligner font référence à la même réplique chez VanDaele :

![Problème expliqué](image_code_couleur/violet/probleme_violet_explained.png)

Dans ce cas-là, on "intègre" les deux répliques en trop dans la réplique alignée.

On écrit les noms des locuteurs du traducteur à aligner à la suite, séparés par "///", n'oubliez pas de revenir à la ligne (crtl + enter) pour chaque "///" pour plus de lisibilité.

Et on fait de même avec les textes :

![Écrire dans cellule violet](image_code_couleur/violet/ecrire_dans_cellule_violet.png)

Ensuite, on supprime les cellules que l'on vient d'ajouter dans celle du dessus :

![Déplacer violet](image_code_couleur/violet/deplacer_violet.png)

![Supprimer violet](image_code_couleur/violet/supprimer_violet.png)

et on applique l'arrière-plan "Magenta clair 2" pour les deux cellules :

![Couleur violet](image_code_couleur/violet/couleur_violet.png)

Et le résultat ressemble à ça :

![Résultat violet](image_code_couleur/violet/resultat_violet.png)

# Rose

Une prise de parole supplémentaire dans la traduction à aligner par rapport à la version de Coulon/VanDaele.

"**Magenta clair 4**" dans Libre Office Calc.

---

Visuel du problème dans OpenOffice Calc :

![Problème rose](image_code_couleur/rose/probleme_rose.png)

Dans cet exemple, on voit que, comme dans le cas Violet, les répliques du locuteur à aligner sont une ligne en dessous par rapport à celle de VanDaele :

![Problème rose expliqué](image_code_couleur/rose/probleme_rose_explained.png)

Donc, comme dans le cas bleu, on va remonter les répliques de la locutions d'une ligne en ajoutant la ligne du CHOEUR dans celle du dessus pour rétablir l'alignement.
Comme pour le cas Violet, on sépare les locuteurs et les répliques par "///" :

![Pré-solution rose](image_code_couleur/rose/pre_solution_rose.png)

A la différence du cas violet, ici, la réplique du CHOEUR n'existe pas chez VanDaele.

On colorie donc les deux cellules en "Magenta clair 4" :

![Couleur rose](image_code_couleur/rose/couleur_rose.png)

Et le résultat ressemble à ça :

![Solution rose](image_code_couleur/rose/solution_rose.png)

# Marron

Inversion de l'ordre des répliques entre la version de Coulon/VanDaele et celle de la traduction à aligner.

"**Or foncé 2**" dans Libre Office Calc.

---

Visuel du problème dans OpenOffice Calc :

![Problème marron](image_code_couleur/marron/probleme_marron.png)

Comme vous pouvez le constater dans l'exemple ci-dessus. Ce problème n'est pas forcément révélé par l'algorithme. Mais lorsqu'on regarde de plus prêt, on peut constater que les répliques ont effectivement été inversées :

![Problème marron expliqué](image_code_couleur/marron/probleme_marron_explained.png)

Dans ce cas-là, dans la cellule du locuteur de la traduction à aligner, à la suite du nom du locuteur, on écrit un "_" puis on écrit le numéro de la ligne chez Coulon/VanDaele correspondant à la réplique :

![Pré-solution marron](image_code_couleur/marron/pre_solution_marron.png)

Puis on colorie en "Or foncé 2" :

![Couleur marron](image_code_couleur/marron/couleur_marron.png)

Et le résultat ressemble à ça :

![Solution marron](image_code_couleur/marron/solution_marron.png)

Dans le cas (très rare), où un cas Violet se conjuge avec un cas Marron, résolvez la problématique comme suit :

![Solution marron/violet](image_code_couleur/marron/solution_marron_violet.png)
