# Intéropérabilité entre JSON et XML-TEI

## Ajouter les données d'alignement aux fichiers TEI

Afin d'enrichir les fichiers TEI avec les données d'alignements avec le fichier original, il est possible d'ajouter un élément `linkGrp` à la fin du fichier xml, content une liste de `link` reliant les répliques à leur équivalent dans le texte de Coulon et une caractérisation de ce lien :  

- `identique` : La réplique est une traduction directe de la réplique dans Coulon (Vert dans l'outil d'alignement)
- `change_locteur` : La réplique est une traduction mais ce n'est pas la même personne qui le prononce. (Orange dans l'outil d'alignement)
- `divise_replique` :  Une seule réplique dans Coulon est divisée en plusieurs répliques dans la traduction. (Magenta dans l'outil d'alignement)
- `supprime_replique` : La réplique a été supprimée dans la traduction. (Gris dans l'outil d'alignement)
- `rassemble_replique` : Plusieurs répliques de Coulon ont été rassemblées dans une réplique unique. (Bleu dans l'outil d'alignement)
- `cree_replique` : Une réplique a été ajoutée à la traduction. (Rose dans l'outil d'alignement)
- `deplace_replique` :  La réplique a été déplacée dans la traduction. (Marron dans l'outil d'alignement)

Pour cela, il faut lancer le script `alignement_to_tei.py` après avoir précisé en haut du fichier les chemins vers les fichiers :

`$python3 alignement_to_tei.py` (nécessite d'avoir installé Beautiful Soup)

## Mettre à jour les fichiers JSON du site web

Dans le cas où les fichiers XML-TEI serait modifiés, il faut mettre à jour les fichiers sur le site web. Pour cela, il faut lancer le script `tei_to_alignement.py` après avoir précisé en haut du fichier les chemins vers les fichiers :

`$python3 tei_to_alignement.py` (nécessite d'avoir installé Beautiful Soup)

Il faut que les fichiers TEI contiennent déjà les données d'alignement, sinon ajouter les avec le script `alignement_to_tei.py` décrit plus haut. 
