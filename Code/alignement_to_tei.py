import json
import os
from bs4 import BeautifulSoup

### Ce script vise à intégrer les données d'alignement au format JSON dans les fichiers XML-TEI du projet Translatoscope. 

# Author : Fanny Mézard 
# Date : 11/2024
# Remarque : Une partie du code est issue du code de Joseph Beau Regard du même dépôt c'est indiqué dans les fonction. 

# Si le dossier résultat et le dossier source sont identiques, les nouveaux XML remplacent les anciens.
# Attention à ne pas finir par un / 

# A modifier : 
dossier_resultat = '../XML_alignes'
dossier_source = '../XML'
oeuvres = os.listdir(dossier_source)

def numeroter_xml(oeuvre, auteur, fichier):
    """
    Numéroter les prises de paroles dans les fichiers XML, crée un nouveau fichier XML et renvoie un objet
    Beautiful Soup

    :param oeuvre: Nom de l'oeuvre 
    :type oeuvre: str
    :param auteur: Nom de l'auteur
    :type auteur: str
    :returns: Un objet BeautifulSoup contenant le contenu du fichier XML correspondant à l'oeuvre, numéroté
    """
    with open (f'{dossier_source}/{oeuvre}/{fichier}', 'r', encoding="utf-8") as file:
        soup = BeautifulSoup(file, features="xml")
    sps = soup.find_all("sp")
    count_sp = 0

    for sp in sps : 
        sp['n'] = auteur + str(count_sp + 1)
        
        count_sp += 1
    if auteur == "Coulon":
        with open(f'{dossier_resultat}/{oeuvre}/{oeuvre}__{auteur}.xml', 'w+', encoding="utf-8") as file:
            content =  str(soup)
            
            content = content.replace(">\n", ">")
            file.write(content)
    return soup


def create_link_grp(dict_json, oeuvre, auteur):
    """
    Crée un liste de tuple contenant les liens entre une traduction et la version de référence
    et le type de modification apportée.

    link_group = [(Coulon1, VanDaele1, identique), (...)]
    :param dict_json: Données d'alignement issues du JSON crée par l'outil d'alignement
    :type dict_json: dict
    :param oeuvre: Nom de l'oeuvre 
    :type oeuvre: str
    :param auteur: Nom de l'auteur
    :type auteur: str
    :returns: Liste de tuple 
    :rtype: list
    """
    compteur_trad = 0
    link_group = []
    for  sp in dict_json[f'{oeuvre}_{auteur}']:
        # 0077bc65 est un équivalent de vert
        couleur = sp[1]
        if couleur in ["Vert", "0077bc65", ] : 
        # Il faudra gérer dans l'affichage le fait que les speaker sont différents
            compteur_trad +=1
            type_modif = "identique"
            link_group.append(("Coulon"+ str(sp[0]), auteur + str(compteur_trad), type_modif))
        elif couleur in ["Orange", "FFFFDE59"]:
            compteur_trad +=1
            type_modif = "change_locuteur"
            link_group.append(("Coulon"+ str(sp[0]), auteur + str(compteur_trad), type_modif))
        elif couleur == "Bleu" : 
        # plusieurs sources pour une trad
        # Dans le bleu, on aligne en rajoutant du texte vide, on a juste à ignorer ce texte vide
            if sp[3] != "":
                type_modif = "rassemble_repliques"
                compteur_trad +=1
                link_group.append(("Coulon"+ str(sp[0]), auteur + str(compteur_trad), type_modif))
        # FFF7D1D5 est une erreur commise dans excel
        elif couleur == "Magenta" or couleur == "FFF7D1D5":
            type_modif = "divise_replique"
        # Plusieurs trad pour une source 
            for texte in sp[2]:
                if type(sp[2]) != list:
                    print("Une erreur a du se glisser dans les couleurs, vérifiez le json ligne", sp[0])
                    exit()
                compteur_trad +=1
                link_group.append(("Coulon"+ str(sp[0]), auteur + str(compteur_trad), type_modif))
        elif couleur == "Gris":
            link_group.append(("Coulon"+ str(sp[0]), auteur + str(0), "supprime_replique"))
            
        # Incertain, cette couleur apparait dans Les cavaliers Van Daele. 
        elif couleur == "Rose" :
            # Il s'agit d'un ajout, on n'a donc pas de lien avec Coulon, mais on a une réplique en plus
            compteur_trad +=1
        elif couleur == "Marron" :
            # Déplacement
            type_modif = "deplace_replique"
            compteur_trad +=1
            link_group.append(("Coulon"+ str(sp[0]), auteur + str(compteur_trad), type_modif))
        elif couleur == "00000000":
            # n'existe qu'à la fin des fichiers
            pass
        else:
            print("Cette couleur n'est pas connue : " + couleur + " " + oeuvre + " " + auteur)
            break
    return link_group


def encoder_alignement(soup, link_group):
    """Ajoute les données d'alignement à la fin du XML 
    :param soup: Objet BeautifulSoup contenant le XML de l'oeuvre 
    :param link_group: données d'alignement avec l'oeuvre de référence 
    :type link_group: list
    :return: le même objet Beautiful Soup auquel ont été ajoutées les données d'alignement. 
    """
    linkGrp = soup.new_tag("linkGrp")
    for c, v, couleur in link_group:
        link = soup.new_tag("link", target = f"#{c} #{v}", type=couleur)
        linkGrp.append(link)
    body = soup.find("text")
    try :
        body.append(linkGrp)
    except AttributeError:
        TEI = soup.find("TEI")
        TEI.append(linkGrp)
        print("Le fichier n'a pas de balise text")
    return soup



equivalences_names = ()

if __name__ == '__main__':

    for oeuvre in oeuvres : 
        print(f'=={oeuvre}==')
        try:
            os.mkdir(f'{dossier_resultat}/{oeuvre}')
            
        except FileExistsError:
            print(f"Directory '{oeuvre}' already exists.")
        
        # Numéroter l'oeuvre de référence 
        numeroter_xml(oeuvre, "Coulon", f"{oeuvre}__Coulon.xml")
        auteurs = os.listdir(f"{dossier_source}/{oeuvre}")
        auteurs.remove(f"{oeuvre}__Coulon.xml")
        
        for fichier in auteurs :

            auteur = fichier.split("__")
            auteur = auteur[1].split(".")[0]
            path_json = f"../Website/content/{oeuvre}_"
            print(auteur)

            # Ouvrir le json d'alignement, s'il n'existe pas, on passe, l'oeuvre n'a pas été alignée
            try:
                with open (f'{path_json}{auteur}.json', 'r', encoding='utf-8') as json_file : 
                    dict_json = json.load(json_file)

                # Créer les listes d'alignements 
                link_group= create_link_grp(dict_json, oeuvre, auteur)
                soup_trad = numeroter_xml(oeuvre, auteur, fichier)
                # Ajouter les alignements dans la TEI 
                soup_trad = encoder_alignement(soup_trad, link_group)
                with open(f'{dossier_resultat}/{oeuvre}/{fichier}', 'w+', encoding="utf-8") as file:
                        content =  str(soup_trad)
                        
                        content = content.replace(">\n", ">")
                        file.write(content)
            except FileNotFoundError:
                print(f"{oeuvre} de {auteur} n'a pas été alignée (pas de fichier json).")
