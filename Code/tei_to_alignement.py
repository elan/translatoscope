import json
import os
from bs4 import BeautifulSoup
import re

### Ce script vise à recrée les alignements des traductions au format JSON à partir des XML afin de les afficher sur le site du Translatoscope

# Author : Fanny Mézard 
# Date : 11/2024
# Remarque : Une partie du code est issue du code de Joseph Beau Regard du même dépôt c'est indiqué dans les fonction. 

# Indiquer là où l'on veut les nouveaux JSON
dossier_resultat = '../JSON_new'
# Indiquer le chemin des fichier XML
dossier_source = '../XML_alignes'
oeuvres = os.listdir(dossier_source)




# @Joseph : on débarasse le texte de toutes les balises pour rendre l'alignement plus agréable
# Cette fonction est basée sur une fonction similaire dans 1_XML_to_xlsx.ipynb qui a été complétée
def clean_text_content(content):
    text_content = str(content)
    # Remove specified tags
    text_without_tags = re.sub(r'</?p>|<p/>|<speaker.*?</speaker>|<desc.*?</desc>|<unclear.*?</unclear>|<gap[^>]*>|<fw[^>]*>.*?</fw>|<fw [^>]*/>|</?sp>|<foreign[^>]*>|<pb[^>]*>|<note.*?</note>', '', text_content, flags=re.DOTALL)
    # Replace <choice> with content of <corr>
    text_without_tags = re.sub(r'<choice.*?<corr>(.*?)</corr>.*?</choice>', r'\1', text_without_tags, flags=re.DOTALL)
        
    # Replace <subst> with content of <add>
    text_without_tags = re.sub(r'<subst.*?<add>(.*?)</add>.*?</subst>', r'\1', text_without_tags, flags=re.DOTALL)
    text_without_tags = re.sub(r'<sp n=\"[^>]*>', "", text_without_tags)


    return text_without_tags.strip()


# @Joseph, fonction recopiée de 3_xlsx_to_JSON.ipynb
def add_breaks_before_stage_tags(text):
    """
    Process the text to replace "<stage> content </stage>" with "<stage> (content) </stage><br>"
    and conditionally add breaks before <stage>.

    Args:
    - data (list): A list of lists where text data may be present.

    Returns:
    - list: The input data list with processed text.
    """

    # Process the text
    text = text.replace("</stage>", "</stage><br>")
    if '<stage>' in text and not text.startswith('<stage>'):
        text = text.replace("<stage>", "<br> <stage>")
    else:
        text = text.replace("<stage>", "<stage>")
    # Update the data list with processed text
    
    return text

# @Joseph, fonction recopiée de 3_xlsx_to_JSON.ipynb
def remove_consecutive_breaks(text):
    """
    Remove consecutive breaks in the given data list.

    Args:
    - data (list): A list of lists where text data may be present.

    Returns:
    - list: The input data list with consecutive breaks removed.
    """

    # Process the text
    text = text.replace("<br><br>", "<br> ")
    text = text.replace("<br> <br>", "<br> ")
    text = text.replace("<br><br><br>", "<br>")
    # Update the data list with processed text
    
    return text


def replace_color_names(couleur_ligne):
    """Passe de l'attribut descriptif de la traduction à la couleur utilisée pour la création du site.

    :param couleur_ligne: attribut descriptif du phénomène de traduction
    :type link_group: str
    :return: la couleur utilisée pour le site
    :rtype: str
    """

    if couleur_ligne == 'identique':
        couleur_ligne = 'Vert'
    elif couleur_ligne == 'change_locuteur':
        couleur_ligne = 'Orange'
    elif couleur_ligne == 'divise_replique':
        couleur_ligne = 'Magenta'
    elif couleur_ligne == "supprime_replique":
        couleur_ligne = 'Gris'
    elif couleur_ligne == "rassemble_repliques":
        couleur_ligne = 'Bleu'
    elif couleur_ligne == "cree_replique":
        couleur_ligne = 'Rose'
    elif couleur_ligne == "deplace_replique":
        couleur_ligne = 'Marron'
    else:
        pass
        # print('Erreur sur cette ligne :')
        # print(row)
    return couleur_ligne

def nettoyage_texte(texte): 
    """
    Appel des fonctions de nettoyage de Joseph
    """
    texte = clean_text_content(texte)
    texte = add_breaks_before_stage_tags(texte)
    texte = remove_consecutive_breaks(texte)
    return texte


def create_dict(soup, oeuvre, auteur):
    """
    Crée un dictionnaire contenant les données nécessaire pour les JSON lu par le site web Translatoscope
    :param soup: Fichier XML contenant les données d'alignement
    :type soup: objet BeautifulSoup
    :type oeuvre: str
    :type auteur: str
    :rtype: dictionnaire
    """
    dict={}
    entree_dict = []
    i = 0
    # On ne traite pas de la même façon le premier et le deuxième magenta
    couleur_precedente = "None"
    # i est le numéro d'index dans la liste
    for link in soup.find_all("link"):
    
        couleur = replace_color_names(link["type"])

        trad = re.search(r" #([a-zA-Z|_]*\d*)", link["target"])
        try : 
            coulon = soup.find_all("link")[i]["target"]
        except IndexError:
            print("Tout a été aligné")
            return dict            

        if not couleur == "Magenta":
            entree_dict.append([])

            coulon = int(re.search(r"Coulon(\d*)", coulon).group(1))
            entree_dict[i].append(int(i+1))
            entree_dict[i].append(couleur)
            if not couleur == "Gris":
                extrait = soup.find("sp", n=trad.group(1))
                try:
                    
                    texte = nettoyage_texte(str(extrait))
                except AttributeError:
                    print("Attention : le nombre de réplique n'est pas équivalent dans le XML et dans le JSON. ALignement impossible")
                    exit()
                entree_dict[i].append(extrait.speaker.get_text().strip())
                entree_dict[i].append(texte)
        
        # On traite tous les magenta d'un coup, après on les ignore
        if couleur == "Magenta" and not couleur_precedente == "Magenta":
            extrait = soup.find("sp", n=trad.group(1))
            entree_dict.append([])
            coulon = int(re.search(r"Coulon(\d*)", coulon).group(1))
            entree_dict[i].append(int(i+1))
            entree_dict[i].append(couleur)
            # Il ajouter un niveau de liste quand c'est Magenta
            couleur_actuelle = "Magenta" 
            j = 0
            liste_magenta = []
            liste_magenta.append([])
            liste_magenta[j].append(extrait.speaker.get_text().strip())
            texte = nettoyage_texte(extrait)
            liste_magenta[j].append(texte)
            
            while couleur_actuelle == "Magenta":
                try : 
                    link = soup.find_all("link")[i+j+1]
                    if replace_color_names(link["type"]) == "Magenta":
                        j+=1
                        
                        liste_magenta.append([])
                        trad = re.search(r" #([a-zA-Z|_]*\d*)", link["target"])
                        couleur = replace_color_names(link["type"])
                        extrait = soup.find("sp", n=trad.group(1))
                        #liste_magenta[j].append(extrait.speaker.string.strip())
                        # il faut boucler sur les p !! 
                        texte = nettoyage_texte(extrait)
                        liste_magenta[j].append(extrait.speaker.get_text().strip())

                        liste_magenta[j].append(texte)
                        link = soup.find_all("link")[i+j]
                        couleur_actuelle = replace_color_names(link["type"])
                       
                    else : 
                        couleur_actuelle = "Autre"
                except IndexError:
                    print("Magenta en fin de fichier")
                    couleur_actuelle="Fin de fichier"
            entree_dict[i].append(liste_magenta)


        if couleur == 'Bleu':
            try:
                coulon_suivant = soup.find_all("link")[i+1]["target"]
                coulon_suivant = re.search(r"Coulon(\d*)", coulon_suivant).group(1)
                # on ajoute une réplique vide par réplique qui n'existe pas dans la traduction.
                
                nb_repliques_vides = int(coulon_suivant)-int(coulon)-1
                for j in range(0, nb_repliques_vides):
                    entree_dict.append([])
                    entree_dict[i+1].append(int(i+1)+j+1)
                    entree_dict[i+1].append(couleur)
                    entree_dict[i+1].append("")
                    entree_dict[i+1].append("")
                    i+=1
            except IndexError:
                print("Fin du fichier, bleu")
                pass
        if couleur == "Gris":
            entree_dict[i].append("")
            entree_dict[i].append("")
            
        if not (couleur == "Magenta" and couleur_precedente == "Magenta"):
            i+=1
        couleur_precedente = couleur
    dict[f'{oeuvre}_{auteur}'] = entree_dict
    return dict



if __name__ == '__main__':
    for oeuvre in oeuvres: 
            print(f'==={oeuvre}===') 
            auteurs = os.listdir(f"{dossier_source}/{oeuvre}")
            auteurs.remove(f"{oeuvre}__Coulon.xml")
            for auteur in auteurs :
                
                auteur = auteur.replace(f"{oeuvre}__", "").replace(".xml", "")
                print(auteur)
                with open (f'{dossier_source}/{oeuvre}/{oeuvre}__{auteur}.xml', 'r', encoding="utf-8") as file:
                    soup = BeautifulSoup(file, features="xml")
                    dict_alignement = create_dict(soup, oeuvre, auteur)
                
                with open(f'{dossier_resultat}/{oeuvre}_{auteur}.json', 'w+', encoding='utf-8') as file:
                    json.dump(dict_alignement, file)
