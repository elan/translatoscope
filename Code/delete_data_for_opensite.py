import pandas as pd
import os
import json
from bs4 import BeautifulSoup
import re
from openpyxl import load_workbook

def get_unpublishable_authors(csv_path):
    """
    Read a CSV file located in the parent folder and extract author names from the first column 
    where the last column is equal to "non".
    
    Parameters:
    csv_path (str): The path to the CSV file in the parent folder.
    
    Returns:
    set: A set of author names from the XML filenames where the last column is "non".
    """
    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(csv_path)
    
    # Create an empty list to store author names
    unpublishable_authors = []

    # Iterate over each row in the DataFrame
    for index, row in df.iterrows():
        # Check if the value in the last column is "non" or empty
        if row.iloc[-1] == "non" or row.iloc[-1] == "":
            # Split the string by '__' and take the second part (author_name)
            filename_part = row.iloc[0].split("__")[1]
            # Remove the '.xml' extension
            author_name = filename_part.replace(".xml", "")

            # Append it to the list
            unpublishable_authors.append(author_name)

    # Convert the list to a set to remove duplicates
    unpublishable_authors_set = set(unpublishable_authors)
    
    return unpublishable_authors_set


def remove_XML_files(xml_directory, unpublishable_authors):
    """
    Remove XML files from directories in the specified directory if they are in the list of unpublishable authors.
    
    Parameters:
    xml_directory (str): The path to the directory containing the XML files.
    unpublishable_authors (list): A list of file names that are not publishable.
    """
    # List all directories in the XML directory
    directories = [d for d in os.listdir(xml_directory) if os.path.isdir(os.path.join(xml_directory, d))]

    # Iterate over each directory
    for directory in directories:
        # Get the path to the current directory
        current_directory = os.path.join(xml_directory, directory)
        
        # List all files in the current directory
        files = os.listdir(current_directory)
        
        # Iterate over each file in the directory
        for filename in files:
            # Check if any unpublishable author is mentioned in the filename
            for author in unpublishable_authors:
                if author in filename:
                    # Remove the file if any unpublishable author is mentioned
                    os.remove(os.path.join(current_directory, filename))
                    print(f"Deleted file: {filename}")


def remove_XLSX_files(xlsx_directory, unpublishable_authors):
    """
    Remove XLSX files from directories in the specified directory if they contain the name of an unpublishable author.
    
    Parameters:
    xlsx_directory (str): The path to the directory containing the XLSX files.
    unpublishable_authors (set): A set of author names that are not publishable.
    """
    # List all directories in the XLSX directory
    directories = [d for d in os.listdir(xlsx_directory) if os.path.isdir(os.path.join(xlsx_directory, d))]

    # Iterate over each directory
    for directory in directories:
        # Get the path to the current directory
        current_directory = os.path.join(xlsx_directory, directory)
        
        # List all files in the current directory
        files = os.listdir(current_directory)
        
        # Iterate over each file in the directory
        for filename in files:

            # Check if any unpublishable author is mentioned in the filename
            for author in unpublishable_authors:
                if author in filename:
                    # Remove the file if any unpublishable author is mentioned
                    os.remove(os.path.join(current_directory, filename))
                    break  # No need to check further, file is already removed

def delete_text_Coulon_in_xlsx_files(alignement_directory):
    """
    Erase the text in the first two columns of each XLSX file in the 'Alignement' directory and its subdirectories.
    
    Parameters:
    alignement_directory (str): The path to the 'Alignement' directory.
    """
    # List all directories in the 'Alignement' directory
    for subdir in os.listdir(alignement_directory):
        subdir_path = os.path.join(alignement_directory, subdir)
        
        # Check if it's a directory
        if os.path.isdir(subdir_path):
            # List all files in the current directory
            for filename in os.listdir(subdir_path):
                if filename.endswith('.xlsx'):
                    full_path = os.path.join(subdir_path, filename)
                    wb = load_workbook(full_path)
                    ws = wb.active

                    for index, row in enumerate(ws.iter_rows(min_row=2, max_row=ws.max_row, values_only=False), start=2):
                        # Erase text in the first column
                        if row[0].value is not None:
                            row[0].value = ""
                        
                        # Erase text in the second column
                        if row[1].value is not None:
                            row[1].value = ""

                    # Save the workbook with the corrected values
                    wb.save(full_path)
                    print(f"Processed {filename}")





def remove_author_in_content_json(content_json_path, unpublishable_authors):
    """
    Remove the JSON files containing unpublishable authors from the content folder.
    
    Parameters:
    content_json_path (str): The path to the "content" folder
    unpublishable_authors (set): A set of author names that are not publishable.
    """
    # List all files in the specified directory
    files = os.listdir(content_json_path)
    
    # Iterate over each file in the directory
    for filename in files:
        # Check if the file has a .json extension
        if filename.endswith('.json'):
            # Check if any unpublishable author is mentioned in the filename
            for author in unpublishable_authors:
                if author in filename:
                    # Remove the file if any unpublishable author is mentioned
                    os.remove(os.path.join(content_json_path, filename))
                    print(f"Deleted file: {filename}")

def remove_author_from_html_dropdown_button(html_path, unpublishable_authors):
    """
    Remove the unpublishable authors from the HTML dropdown button.
    
    Parameters:
    html_path (str): The path to the HTML file.
    unpublishable_authors (set): A set of author names that are not publishable.
    """
    # Read the HTML content from the file
    with open(html_path, 'r', encoding='utf-8') as file:
        html_content = file.read()

    # Define a regex pattern to match the <li> elements
    pattern = re.compile(r'(<li class="dropdown-item">.*?</li>)', re.DOTALL)
    
    # Function to check if an author is unpublishable and should be commented out
    def comment_unpublishable_author(match):
        li_content = match.group(1)
        for author in unpublishable_authors:
            if author in li_content:
                return f'<!-- {li_content} This option has been removed because the text in not in the public domain -->'
        return li_content

    # Comment out the unpublishable authors from the HTML content
    modified_html_content = re.sub(pattern, comment_unpublishable_author, html_content)

    # Write the updated HTML content back to the file
    with open(html_path, 'w', encoding='utf-8') as file:
        file.write(modified_html_content)

def remove_other_plays_from_html_dropdown_button(html_path, play_to_keep):
    """
    Remove the "dropdown-submenu" sections that are not in the play_to_keep list from the HTML file.
    
    Parameters:
    html_path (str): The path to the HTML file.
    play_to_keep (list): A list of play IDs to keep in the dropdown menu.
    """
    # Read the HTML content from the file
    with open(html_path, 'r', encoding='utf-8') as file:
        html_content = file.read()

    # Split the HTML content by lines for easier manipulation
    html_lines = html_content.splitlines()

    # Initialize variables for tracking the lines to remove
    inside_dropdown_submenu = False
    lines_to_remove = set()

    # Iterate over each line in the HTML content
    for i, line in enumerate(html_lines):
        # Check if the line contains the start of a "dropdown-submenu"
        if 'class="dropdown-submenu' in line:
            # Extract the play ID from the line
            play_id = line.split('id="')[1].split('"')[0]
            # Check if the play ID is not in the play_to_keep list
            if play_id not in play_to_keep:
                inside_dropdown_submenu = True
                lines_to_remove.add(i)
        # If inside a "dropdown-submenu" section, continue adding lines to remove
        elif inside_dropdown_submenu:
            lines_to_remove.add(i)
            # Check if the line contains the end of a "dropdown-submenu" section
            if '</ul>' in line:
                inside_dropdown_submenu = False
                # Also include the closing </li> line after the </ul>
                lines_to_remove.add(i + 1)

    # Remove the identified lines
    html_lines = [line for i, line in enumerate(html_lines) if i not in lines_to_remove]

    # Join the remaining lines back into a single string
    new_html_content = '\n'.join(html_lines)

    # Write the modified HTML content back to the file
    with open(html_path, 'w', encoding='utf-8') as file:
        file.write(new_html_content)


def remove_author_from_html_bibliographie(html_path, unpublishable_authors):
    """
    Remove the unpublishable authors from the HTML bibliographie.
    
    Parameters:
    html_path (str): The path to the HTML file.
    unpublishable_authors (set): A set of author names that are not publishable.
    """
    # Read the HTML content from the file
    with open(html_path, 'r', encoding='utf-8') as file:
        html_content = file.read()

    # Define a regex pattern to match the <li> elements in the bibliographie
    pattern = re.compile(r'(<bib>.*?</bib>)', re.DOTALL)

    
    
    # Function to check if an author is unpublishable and should be commented out
    def comment_unpublishable_author(match):
        bib_content = match.group(1)
        for author in unpublishable_authors:
            if author in bib_content:
                return f'<!-- {bib_content} This line has been removed because the text in not in the public domain -->'
        return bib_content

    # Comment out the unpublishable authors from the HTML content
    modified_html_content = re.sub(pattern, comment_unpublishable_author, html_content)

    # Write the updated HTML content back to the file
    with open(html_path, 'w', encoding='utf-8') as file:
        file.write(modified_html_content)


def change_loading_text_in_alignement_javascript(alignement_javascript_path):
    """
    Change the text that loads as default from the text from Ploutus_Coulon to Ploutus_VanDaele in the alignement.js file.
    
    Parameters:
    alignement_javascript_path (str): The path to the alignement_javascript file.
    """
    # Open the file in read mode
    with open(alignement_javascript_path, 'r') as file:
        lines = file.readlines()

    # Iterate through each line in the file
    for i, line in enumerate(lines):
        # Check if the line contains the text to replace
        if 'this.addNewCard("Ploutos", "Ploutos", "1930 - Coulon")' in line:
            # Replace the line with the new text
            lines[i] = 'this.addNewCard("Ploutos","Ploutos","1930 - VanDaele")\n'

    # Write the modified lines back to the file
    with open(alignement_javascript_path, 'w') as file:
        file.writelines(lines)


def main():
    csv_path = "../droit_ouverture.csv"

    # Obtiens une liste de noms de fichiers non publiables à partir du fichier csv "droit_ouverture.csv"
    unpublishable_authors = get_unpublishable_authors(csv_path)

    # on retire les fichiers XML des auteurs qui ne sont pas dans le domaine public
    remove_XML_files("../XML/", unpublishable_authors)

    # on retire les fichiers XSLS des auteurs qui ne sont pas dans le domaine public
    remove_XLSX_files("../Alignement/", unpublishable_authors)

    # on enlève le texte de Coulon dans les fichiers XLSX
    delete_text_Coulon_in_xlsx_files("../Alignement/")

    # on retire les entrées des auteurs qui ne sont pas dans le domaine public
    remove_author_in_content_json('../Website/content/', unpublishable_authors)

    # on retire les entrées du dropdown button de choix des textes dans alignement.html pour les auteurs qui ne sont pas dans le domaine public
    remove_author_from_html_dropdown_button('../Website/alignement.html', unpublishable_authors)


    play_to_keep = ["Ploutos"]
    # Remove the plays from the HTML dropdown button
    remove_other_plays_from_html_dropdown_button("../Website/alignement.html", play_to_keep)

    # on retire les références bibliographiques dans source.html pour les auteurs qui ne sont pas dans le domaine public
    remove_author_from_html_bibliographie('../Website/source.html', unpublishable_authors)



    # On change le texte qui charge par défaut (au chargement de la page alignement.html) de Ploutos_Coulon en Ploutos_VanDaele dans alignement.js
    change_loading_text_in_alignement_javascript("../Website/assets/js/alignement.js")

if __name__ == "__main__":
    main()